# Node.js Typescript boilerplate

### Features
* TypeScript
* ESLint
* Prettier
* Jest
* Gitlab CI
* EditorConfig
* Dotenv
* VSCode debug config
